package dev.perez.data;

import dev.perez.models.Location;
import dev.perez.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class LocationDaoHibImpl implements LocationDao {

    @Override
    public List<Location> getAllLocations() {
        return null;
    }

    @Override
    public Location createLocation(Location newLocation) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            int id = (int) s.save(newLocation);
            newLocation.setId(id);
            tx.commit();
            return newLocation;
        }
    }

}
