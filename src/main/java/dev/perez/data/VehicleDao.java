package dev.perez.data;

import dev.perez.models.Vehicle;

import java.util.List;

public interface VehicleDao {

    public List<Vehicle> getAllVehicles();
    public Vehicle getVehicleById(int id);
    public Vehicle addNewVehicle(Vehicle vehicle);
    public void deleteVehicle(int id);
    public Vehicle updateVehicle(Vehicle newVehicle);
    public List<Vehicle> getVehiclesInPriceRange(double min, double max);
    public List<Vehicle> getVehiclesWithMaxPrice(double max);
    public List<Vehicle> getVehiclesWithMinPrice(double min);


}
