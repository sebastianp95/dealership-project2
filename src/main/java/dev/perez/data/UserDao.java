package dev.perez.data;

import dev.perez.models.User;

public interface UserDao {

    public User authenticateUser(String username, String password);

    public User getUserByUsername(String username);

    public User addNewUser(User user);
}
