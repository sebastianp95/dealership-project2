package dev.perez.controllers;

import dev.perez.models.AuthenticationResponse;
import dev.perez.models.User;
import dev.perez.services.UserService;
import dev.perez.util.JwtUtil;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthController {

    private Logger logger = LoggerFactory.getLogger(AuthController.class);
    private UserService service = new UserService();
    private JwtUtil jwtTokenUtil = new JwtUtil();
    private AuthenticationResponse authenticationResponse;
    String token ;
    public void authenticateLogin(Context ctx){
        // getting params from what would be a form submission (Content-Type: application/x-www-form-urlencoded)
        String user = ctx.formParam("username");
        String pass = ctx.formParam("password");

        User userDetails = service.authenticate(user , pass);


        if(userDetails!=null) {
            logger.info("successful login");

            token = jwtTokenUtil.generateToken(userDetails);
            authenticationResponse = new AuthenticationResponse(token);
            ctx.header("Authorization", token);
            ctx.status(200);
//            return ResponseEntity.ok(new AuthenticationResponse(token));
            return;
        }
        logger.info("Credentials were incorrect");
        throw new UnauthorizedResponse("Credentials were incorrect");
    }

    public void authorizeToken(Context ctx) {
        logger.info("attempting to authorize token");

        if (ctx.method().equals("OPTIONS")) {
            return;
        }
        // getting the "Authorization" header from the incoming request
        String authHeader = ctx.header("Authorization");

        if (authHeader != null) {
            String username = jwtTokenUtil.extractUsername(authHeader);
            User userDetails = this.service.loadUserByUsername(username);

            if (jwtTokenUtil.validateToken(authHeader, userDetails)) {
                logger.info("validate token successful");
                //Assign role rules with access manager
            }
//        if(authHeader!=null && authHeader.equals("admin-auth-token")){
//            logger.info("validate token successful");
//        }
            else {
                logger.warn("improper authorization");
                throw new UnauthorizedResponse("token was incorrect");
            }
        }

    }
}
