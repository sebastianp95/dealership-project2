<div align=”center”>
<p align="center">
  <a href="https://gitlab.com/sebastianp95/dealership-project">
    <img src="images/readmelogo.png" alt="Logo" width="80" height="80">
  </a>
 

  <h2 align="center">DealershipAPI</h2>

  <p align="center">
    Free API for car inventory management
</p>
</div>

<!-- TABLE OF CONTENTS -->

<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#routes">Routes</a></li>
  <li><a href="#acknowledgements">Acknowledgements</a></li>

  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project

Dealership is a free online REST API that can be used by car companies to manage their inventory. It implements
 authentication and token authorization features.

### Built With

* [Java](https://www.java.com/en/)
* [Javalin](https://javalin.io/)
* [PostgreSQL](https://www.postgresql.org/)
* [Azure](https://azure.microsoft.com/en-us/)



<!-- GETTING STARTED -->
## Getting Started

You can add Dealership to an HTML page or invoke its endpoints with a client at rest client

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/sebastianp95/dealership-project.git
   ```
2. Build your gradle file
 * [Gradle](https://gitlab.com/sebastianp95/dealership-project/-/blob/master/build.gradle)
3. Configure your database credentials as environment variables and you're good to go



## Schema

![GitHub Logo](/images/schema.PNG)
  
 
<!-- ROUTES-->

## Routes

* GET	 /items
* GET	 /items/1
* GET	 /items?max-price=115000
* GET	 /items?min-price=95000
* GET    /items?max-price=115000&min-price=95000
* POST   /items
* POST   /login
* PUT	 /items
* DELETE /items/1

<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [JWT](https://jwt.io/)
* [Mockito](https://site.mockito.org/)
* [Junit](https://junit.org/junit5/)
